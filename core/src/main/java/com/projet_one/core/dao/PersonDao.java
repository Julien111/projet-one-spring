package com.projet_one.core.dao;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.projet_one.core.model.Person;

//on a deux méthodes une qui va insérer une personne avec un identifiant
//l'autre génère un identifiant aléatoire et utilise insertPerson

public interface PersonDao {
    
    int insertPerson(UUID id, Person person);

    default int insertPerson(Person person){
        UUID id = UUID.randomUUID();
        return insertPerson(id, person);
    }

    List<Person> selectAllPeople();

    Optional<Person> selectPersonById(UUID id);

    int deletePersonById(UUID id);

    int updatePersonById(UUID id, Person person);
    
}
